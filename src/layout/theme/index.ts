/**
 * @description ⚠️：此文件仅供主题插件使用，请不要在此文件中导出别的工具函数（仅在页面加载前运行）
 */

import { type multipleScopeVarsOptions } from "@pureadmin/theme";

/** 预设主题色 */
const themeColors = {
  default: {
    subMenuActiveText: "#fff",
    menuBg: "#273040",
    menuHover: "#ffb8b1",
    subMenuBg: "#a7a9af",
    subMenuActiveBg: "#4091f7",
    menuText: "rgb(254 254 254 / 65%)",
    sidebarLogo: "#273040",
    menuTitleHover: "#fff",
    menuActiveBefore: "#ff805c"
  },
  light: {
    subMenuActiveText: "#fff",
    menuBg: "#10295a",
    menuHover: "#10295a",
    subMenuBg: "#1fa9ed52",
    subMenuActiveBg: "#10295a",
    menuText: "#fff",
    sidebarLogo: "#10295a",
    menuTitleHover: "#fff",
    menuActiveBefore: "#1fa9ed"
  },
  volcano: {
    subMenuActiveText: "#fff",
    menuBg: "#008b8d",
    menuHover: "#e85f33",
    subMenuBg: "#cdeea3",
    subMenuActiveBg: "#712e6e",
    menuText: "rgb(255 255 255 / 75%)",
    sidebarLogo: "#008b8d",
    menuTitleHover: "#fff",
    menuActiveBefore: "#e85f33"
  },
  business: {
    subMenuActiveText: "#fff",
    menuBg: "linear-gradient(to top, #00923f 1%, #0177bd 70% )",
    menuHover: "#e13c39",
    subMenuBg: "rgb(238 241 247 / 25%)",
    subMenuActiveBg: "#e13c39",
    menuText: "rgb(254 254 254 / 65.1%)",
    sidebarLogo: "#0177bd",
    menuTitleHover: "#fff",
    menuActiveBefore: "#089843"
  },
};

/**
 * @description 将预设主题色处理成主题插件所需格式
 */
export const genScssMultipleScopeVars = (): multipleScopeVarsOptions[] => {
  const result = [] as multipleScopeVarsOptions[];
  Object.keys(themeColors).forEach(key => {
    result.push({
      scopeName: `layout-theme-${key}`,
      varsContent: `
        $subMenuActiveText: ${themeColors[key].subMenuActiveText} !default;
        $menuBg: ${themeColors[key].menuBg} !default;
        $menuHover: ${themeColors[key].menuHover} !default;
        $subMenuBg: ${themeColors[key].subMenuBg} !default;
        $subMenuActiveBg: ${themeColors[key].subMenuActiveBg} !default;
        $menuText: ${themeColors[key].menuText} !default;
        $sidebarLogo: ${themeColors[key].sidebarLogo} !default;
        $menuTitleHover: ${themeColors[key].menuTitleHover} !default;
        $menuActiveBefore: ${themeColors[key].menuActiveBefore} !default;
      `
    } as multipleScopeVarsOptions);
  });
  return result;
};
