export default {
  path: "/test",
  // name: "test",
  // component: Layout,
  redirect: "/test/index",
  meta: {
    icon: "arrowRightCircleFill",
    title: "测试页面目录",
    rank: 2
  },
  children: [
    {
      path: "/test/index",
      name: "test",
      component: () => import("@/views/permission/page/test.vue"),
      meta: {
        title: "测试页面",
      }
    }
  ]
} as RouteConfigsTable;
